//
//  ETStoryboards.swift
//  ET
//
//  Created by HungNguyen on 10/30/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import UIKit

struct ETStoryboard {
    
    static let main   = UIStoryboard(name: "Main", bundle: nil)
    static let items  = UIStoryboard(name: "Items", bundle: nil)
    
}
