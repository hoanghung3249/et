//
//  Reusable.swift
//  ET
//
//  Created by HungNguyen on 10/30/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import UIKit

protocol Reusable {
    static var reuseID: String {get}
}

extension Reusable {
    static var reuseID: String {
        return String(describing: self)
    }
}

extension UITableViewCell: Reusable {}
extension UICollectionViewCell: Reusable {}

extension UIViewController: Reusable {}

extension UITableView {
    /// Register a UITableViewCell which using a nib file
    ///
    /// - Parameter cell: Type of UITableViewCell class
    func registerNib<T: UITableViewCell>(forCell cell: T.Type) {
        register(UINib(nibName: T.reuseID, bundle: nil), forCellReuseIdentifier: T.reuseID)
    }
    
    /// Register a UITableViewCell which only using a class file
    ///
    /// - Parameter cell: Type of UITableViewCell class
    func registerClass<T: UITableViewCell>(forCell cell: T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseID)
    }
    
    func dequeueReusableCell<T>(ofType cellType: T.Type = T.self, at indexPath: IndexPath) -> T where T: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: cellType.reuseID,
                                             for: indexPath) as? T else {
                                                fatalError()
        }
        return cell
    }
}

extension UICollectionView {
    /// Register a UICollectionViewCell which using a nib file
    ///
    /// - Parameter cell: Type of UICollectionViewCell class
    func registerNib<T: UICollectionViewCell>(forCell cell: T.Type) {
        register(UINib(nibName: T.reuseID, bundle: nil), forCellWithReuseIdentifier: T.reuseID)
    }
    
    /// Register a UICollectionViewCell which only using a class file
    ///
    /// - Parameter cell: Type of UICollectionViewCell class
    func registerClass<T: UICollectionViewCell>(forCell cell: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.reuseID)
    }
    
    func dequeueReusableCell<T>(ofType cellType: T.Type = T.self, at indexPath: IndexPath) -> T where T: UICollectionViewCell {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cellType.reuseID, for: indexPath) as? T else {
            fatalError()
        }
        return cell
    }
}

extension UIStoryboard {
    func instantiateViewController<T>(ofType type: T.Type = T.self) -> T where T: UIViewController {
        guard let viewController = instantiateViewController(withIdentifier: type.reuseID) as? T else {
            fatalError()
        }
        return viewController
    }
}
