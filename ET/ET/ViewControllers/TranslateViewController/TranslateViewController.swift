//
//  TranslateViewController.swift
//  ET
//
//  Created by HungNguyen on 10/30/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import UIKit
import AVFoundation

class TranslateViewController: BaseViewController {
    @IBOutlet weak var vwAction: ActionView!
    @IBOutlet weak var vwLanguage: LanguageView!
    @IBOutlet weak var txvFromLanguage: UITextView!
    @IBOutlet weak var txvToLanguage: UITextView!
    @IBOutlet weak var btnTranslate: UIButton!
    @IBOutlet weak var btnRemoveTextFromLanguage: UIButton!
    @IBOutlet weak var btnSpeakerFromLanguage: UIButton!
    @IBOutlet weak var btnSpeakerToLanguage: UIButton!
    
    var viewModel = TranslateViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwAction.resetActionButton.onNext(())
    }
    
    override func redrawLayout() {
        setupButtonInTextView()
    }
    
    override func bindingViewModel() {
        bindCommonAction(viewModel)
        //        txvFromLanguage.rx.text.bind(to: viewModel.translateText).disposed(by: disposeBag)
        
        (txvFromLanguage.rx.text <-> viewModel.translateText).disposed(by: disposeBag)
        
        viewModel.selectLanguageView.selectedLanguage.bind(to: vwLanguage.selectedLanguage).disposed(by: disposeBag)
        
        vwLanguage.translateModel.asObservable()
            .subscribe(onNext: { [weak self] (model) in
                guard let self = self else { return }
                self.viewModel.translateModel = model
                self.viewModel.checkingSupportedVoice(model)
            }).disposed(by: disposeBag)
        
        vwLanguage.swapLanguageBehavior.bind(to: viewModel.swapLanguageBehavior).disposed(by: disposeBag)
        
        viewModel.finalText.asDriver().drive(txvToLanguage.rx.text).disposed(by: disposeBag)
        
        viewModel.fromLanguageBehavior.bind(to: vwLanguage.fromLanguageBehavior).disposed(by: disposeBag)
        viewModel.toLanguageBehavior.bind(to: vwLanguage.toLanguageBehavior).disposed(by: disposeBag)
        
        // Hide/Show Speaker button
        viewModel.isShowSpeakerFromLng.asDriver().drive(btnSpeakerFromLanguage.rx.isHidden).disposed(by: disposeBag)
        viewModel.isShowSpeakerToLng.asDriver().drive(btnSpeakerToLanguage.rx.isHidden).disposed(by: disposeBag)
    }
    
    override func observeSignal() {
        txvFromLanguage.rx.text.asDriver()
            .drive(onNext: { [weak self] (text) in
                guard let self = self, let text = text else { return }
                self.btnRemoveTextFromLanguage.isHidden = text.isEmpty
            }).disposed(by: disposeBag)
        
        btnRemoveTextFromLanguage.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.txvFromLanguage.text = ""
            }).disposed(by: disposeBag)
        
        vwLanguage.selectLanguageSignal
            .subscribe(onNext: { [weak self] (index, language) in
                guard let self = self else { return }
                self.viewModel.loadSelectLanguageView(in: self.view, index, language)
            }).disposed(by: disposeBag)
        
        btnTranslate.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.requestTranslateText()
            }).disposed(by: disposeBag)
        
        vwAction.selectedSender.subscribe(onNext: { [weak self] (tag) in
            guard let self = self else { return }
            switch tag {
            case 1: self.presentViewCamera()
            default: break
            }
        }).disposed(by: disposeBag)
        
        btnSpeakerFromLanguage.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self, let text = self.txvFromLanguage.text else { return }
                self.viewModel.textToSpeech(text, self.viewModel.translateModel.sourceLanguage)
            }).disposed(by: disposeBag)
        
        btnSpeakerToLanguage.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self, let text = self.txvToLanguage.text else { return }
                self.viewModel.textToSpeech(text, self.viewModel.translateModel.target)
            }).disposed(by: disposeBag)
    }
    
}

// MARK: - Support Method
private extension TranslateViewController {
    
    func setupButtonInTextView() {
        // Get the button frame from storyboard
        let buttonFrame = btnRemoveTextFromLanguage.frame
        // Convert button's frame to path
        let exclusivePath = UIBezierPath(rect: buttonFrame)
        
        // Add path to the textview in main queue
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.txvFromLanguage.textContainer.exclusionPaths = [exclusivePath]
        }
    }
    
    func presentViewCamera() {
        let cameraVC = ETStoryboard.main.instantiateViewController(ofType: CameraViewController.self)
        cameraVC.hidesBottomBarWhenPushed = true
        cameraVC.viewModel.reloadLanguage.bind(to: viewModel.reloadLanguage).disposed(by: cameraVC.viewModel.disposeBag)
        navigationPushView(cameraVC)
    }
    
}
