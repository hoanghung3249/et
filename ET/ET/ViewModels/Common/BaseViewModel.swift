//
//  BaseViewModel.swift
//  ET
//
//  Created by HungNguyen on 10/30/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Firebase
import VisionKit
import Vision
import AVFoundation

class BaseViewModel {
    let disposeBag = DisposeBag()
    let shouldReloadData = BehaviorRelay<Void?>(value: nil)
    let shouldReloadSections = BehaviorRelay<IndexSet?>(value: nil)
    let shouldReloadRows = BehaviorRelay<[IndexPath]?>(value: nil)
    lazy var activityIndicator = ActivityIndicator() // For checking loading status
    var requestProcessTracking: [PublishSubject<Void>] = [] // For tracking multithreads request
    let didRequestError = BehaviorRelay<Error?>(value: nil)
    let fromLanguageBehavior = BehaviorRelay<String>(value: "")
    let toLanguageBehavior = BehaviorRelay<String>(value: "")
    let swapLanguageBehavior = PublishRelay<Void>()
    let selectedImage = PublishSubject<UIImage>()
    
    let isSupportedVoiceFromLng = BehaviorRelay<Bool>(value: true)
    let isSupportedVoiceToLng = BehaviorRelay<Bool>(value: true)
    let isShowSpeakerFromLng = BehaviorRelay<Bool>(value: true)
    let isShowSpeakerToLng = BehaviorRelay<Bool>(value: true)
    
    // Views
    var selectLanguageView: ListLanguageView!
    
    // Detect text from image
    let textFromImage = PublishRelay<String>()
    
    // MARK: - Init BaseViewModel
    init() {
        TranslationManager.shared.loadListSupportedLanguage()
        firstSetupLanguage()
    }
    
    deinit {
        print("Deinit ViewModel: \(type(of: self))")
    }
    
    /// Clear all data for refresh
    func clearAll() {} // Override
    
    /// Refresh data
    func refreshData() {
        clearAll()
    }
    
    func requestError(_ error: Error) {
        didRequestError.accept(error)
    }
    
    func reloadData() { shouldReloadData.accept(()) }
    
    func reloadSections(_ sections: IndexSet) { shouldReloadSections.accept(sections) }
    
    func reloadRows(_ rows: [IndexPath]) { shouldReloadRows.accept(rows) }
    
}

// ======================================================================
// MARK: - Request Tracking Flow
// ======================================================================
extension BaseViewModel {
    func clearRequestTracking() {
        requestProcessTracking = []
    }
    
    func requestTrackingCompletedAt(_ index: Int) {
        requestProcessTracking[safe: index]?.onCompleted()
    }
    
    func requestTrackingErrorAt(_ index: Int, error: Error) {
        requestProcessTracking[safe: index]?.onError(error)
    }
}

// MARK: - Support Method
extension BaseViewModel {
    
    func firstSetupLanguage() {
        // Load current device's language
        let currentLanguage = TranslationManager.shared.supportedLanguage.filter({$0.language == TranslationManager.shared.getDefaultLanguage() }).first
        
        if getDefaultLanguage(of: .fromLanguage) == nil && getDefaultLanguage(of: .toLanguage) == nil {
            // If default language nil, save the current language
            TranslationManager.shared.saveLanguage(type: .fromLanguage(currentLanguage ?? LanguageModel("English", "en")))
            TranslationManager.shared.saveLanguage(type: .toLanguage(currentLanguage ?? LanguageModel("English", "en")))
            fromLanguageBehavior.accept(currentLanguage?.name ?? "")
            toLanguageBehavior.accept(currentLanguage?.name ?? "")
        } else {
            // Load the last default language
            let fromLanguage = TranslationManager.shared.loadLanguage(for: .fromLanguage)
            let toLanguage = TranslationManager.shared.loadLanguage(for: .toLanguage)
            fromLanguageBehavior.accept(fromLanguage?.name ?? "")
            toLanguageBehavior.accept(toLanguage?.name ?? "")
        }
    }
    
    func getDefaultLanguage(of type: UserDefaultKey) -> LanguageModel? {
        guard let model = TranslationManager.shared.loadLanguage(for: type) else { return nil }
        return model
    }
    
    /// Load ListLanguageView into current view
    ///
    ///    - Parameters:
    ///        - view: Current View
    ///        - selectedIndex: The index (1 or 2) of LanguageView (1: From Language - 2: To Language)
    ///        - selectedLanguage: The LanguageModel
    ///
    func loadSelectLanguageView(in view: UIView, _ selectedIndex: Int, _ selectedLanguage: LanguageModel) {
        view.addSubview(selectLanguageView)
        selectLanguageView.selectedIndex = selectedIndex
        selectLanguageView.inputLanguage.accept(selectedLanguage)
        selectLanguageView.scaleEqualSuperView()
        selectLanguageView.animateDetailView(true)
    }
    
    /// Detect text in image
    ///
    /// - Parameters:
    ///     - image: The image input
    func detectText(from image: UIImage) {
        // Scale image
//        let scaledImage = image.scaledImage(1000) ?? image
        // Improve image quality
//        let preprocessedImage = scaledImage.preprocessedImage() ?? scaledImage
        
//        if #available(iOS 13.0, *) {
//            detectTextiOS13(from: preprocessedImage)
//        } else {
        detectText(in: image)
//        }
    }
    
    private func detectText(in image: UIImage) {
        print("Detect text using: Google Cloud Vision")

        ETServiceManager.shared.requestImage(image).trackActivity(activityIndicator)
            .subscribe(onNext: { [weak self] (text) in
                guard let self = self else { return }
                print("========= Text in image: \(text)")
                self.textFromImage.accept(text)
            }, onError: { [weak self] (error) in
                guard let self = self else { return }
                self.requestError(error)
            }).disposed(by: disposeBag)
    }
    
    private func detectTextiOS13(from image: UIImage) {
        print("Detect text using: VNRecognizeTextRequest")
        
        ETServiceManager.shared.detectTextiOS13(in: image).trackActivity(activityIndicator)
            .subscribe(onNext: { [weak self] (text) in
                guard let self = self else { return }
                print("========= Text in image: \(text)")
                self.textFromImage.accept(text)
            }, onError: { [weak self] (error) in
                guard let self = self else { return }
                self.requestError(error)
            }).disposed(by: disposeBag)
    }
    
    /// Checking the supported voice form Apple's API
    ///
    /// - Parameters:
    ///     - translateModel: TranslateModel
    func checkingSupportedVoice(_ translateModel: TranslateModel) {
        let isFromLngSupported = TranslationManager.shared.supportedVoice.contains(where: {$0.getLanguageCode() == translateModel.sourceLanguage})
        let isToLngSupported = TranslationManager.shared.supportedVoice.contains(where: {$0.getLanguageCode() == translateModel.target})
        isSupportedVoiceFromLng.accept(isFromLngSupported)
        isSupportedVoiceToLng.accept(isToLngSupported)
    }
    
    /// Convert text to speech using Apple's API
    ///
    /// - Parameters:
    ///     - text: The text input
    ///     - language: The language (Ex: en)
    func textToSpeech(_ text: String, _ language: String){
        // Create an instance of AVSpeechSynthesizer.
        let speechSynthesizer = AVSpeechSynthesizer()
        // Create an instance of AVSpeechUtterance and pass in a String to be spoken.
        let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: text)
        // Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 4.0
        // Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: language)
        // Pass in the urrerance to the synthesizer to actually speak.
        speechSynthesizer.speak(speechUtterance)
        
    }

}
