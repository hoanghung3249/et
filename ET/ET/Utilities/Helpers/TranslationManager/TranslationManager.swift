//
//  TranslationManager.swift
//  ET
//
//  Created by HungNguyen on 11/12/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import AVFoundation

enum LanguageType {
    case fromLanguage(_ model: LanguageModel)
    case toLanguage(_ model: LanguageModel)
}

enum UserDefaultKey {
    case fromLanguage
    case toLanguage
}

extension UserDefaultKey {
    var key: String {
        switch self {
        case .fromLanguage: return "from"
        case .toLanguage: return "to"
        }
    }
}

class TranslationManager {
    
    static let shared = TranslationManager()
    var supportedLanguage = [LanguageModel]()
    var supportedVoice = [String]()
    
    init() {
        loadListSupportedLanguage()
        loadSupportedVoices()
    }
    
    /// Load default language from device
    func getDefaultLanguage() -> String { return Locale.current.languageCode ?? "en" }
    
    /// Load list supported language from local file (SupportedLanguage.json)
    func loadListSupportedLanguage() {
        var listLanguageSupported: ListLanguageModel?
        let bundle = Bundle.main.path(forResource: "SupportedLanguage", ofType: "json")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: bundle ?? ""), options: .mappedIfSafe)
            listLanguageSupported = ListLanguageModel(JSON: data.toJSON() ?? [String: Any]())
            supportedLanguage = listLanguageSupported?.languages ?? []
        } catch(let error) {
            print("Error when get supported language: \(error.localizedDescription)")
        }
    }
    
    /// Save the new language
    ///
    /// - Parameters:
    ///     - languageType: LanguageType (fromLanguage or toLanguage)
    func saveLanguage(type languageType: LanguageType) {
        switch languageType {
        case .fromLanguage(let model):
            DataManager.shared.saveData(model.toJSON(), UserDefaultKey.fromLanguage.key)
        case .toLanguage(let model):
            DataManager.shared.saveData(model.toJSON(), UserDefaultKey.toLanguage.key)
        }
    }
    
    /// Load the language from key
    ///
    /// - Parameters:
    ///     - type: Key to load language (UserDefaultKey)
    /// - Returns:
    ///     - LanguageModel?
    func loadLanguage(for type: UserDefaultKey) -> LanguageModel? {
        var param: [String: Any]?
        switch type {
        case .fromLanguage:
            param = DataManager.shared.getData(for: UserDefaultKey.fromLanguage.key)
        case .toLanguage:
            param = DataManager.shared.getData(for: UserDefaultKey.toLanguage.key)
        }
        
        guard let params = param else { return nil }
        return LanguageModel(JSON: params)
    }
    
    /// Swap the language (fromLanguage <-> toLanguage)
    func swapLanguage() {
        // Get language
        let fromLanguage = LanguageModel(JSON: DataManager.shared.getData(for: UserDefaultKey.fromLanguage.key) ?? [:])
        let toLanguage = LanguageModel(JSON: DataManager.shared.getData(for: UserDefaultKey.toLanguage.key) ?? [:])
        
        // Swap language
        saveLanguage(type: .fromLanguage(toLanguage ?? LanguageModel("English", "en")))
        saveLanguage(type: .toLanguage(fromLanguage ?? LanguageModel("English", "en")))
    }
    
    /// Load list supported voices from device
    func loadSupportedVoices() {
        supportedVoice = AVSpeechSynthesisVoice.speechVoices().map({ $0.language })
    }
}
