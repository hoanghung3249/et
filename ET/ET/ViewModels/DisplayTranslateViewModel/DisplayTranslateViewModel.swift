//
//  DisplayTranslateViewModel.swift
//  ET
//
//  Created by HungNguyen on 12/20/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DisplayTranslateViewModel: BaseViewModel {
    
    var translateModel = BehaviorRelay<TranslateModel?>(value: nil)
    let finalText = PublishRelay<String>()
    
    init(_ translateModel: TranslateModel) {
        super.init()
        self.translateModel.accept(translateModel)
        translateText(translateModel)
    }
    
    func translateText(_ model: TranslateModel!) {
        ETServiceManager.shared.request(.translate(model), mapObject: DataModel<ListTranslateModel>.self)
            .trackActivity(activityIndicator)
            .subscribe(onNext: { [weak self] (response) in
                guard let self = self, let model = response.data, let translateResponse = model.translations.first else { return }
                self.finalText.accept(translateResponse.translatedText ?? "")
            }, onError: { [weak self] (error) in
                guard let self = self else { return }
                self.requestError(error)
            }).disposed(by: disposeBag)
    }
    
}
