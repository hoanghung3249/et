//
//  ListLanguageView.swift
//  ET
//
//  Created by HungNguyen on 11/8/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ListLanguageView: BaseCustomView {
    
    @IBOutlet weak var vwBound: UIView!
    @IBOutlet weak var vwLanguage: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var tbvListLanguage: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var selectedIndex = 1
    var languageName = ""
    let inputLanguage = PublishRelay<LanguageModel>()
    let selectedLanguage = PublishRelay<(Int, String)>()
    private var listLanguage = [LanguageModel]()
    
    deinit {
        for gesture in vwBound.gestureRecognizers ?? [] { vwBound.removeGestureRecognizer(gesture) }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
        configure()
        observeSignal()
        addGesture()
        prepareLanguage()
        setupTbv()
        setupSearchBar()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
        configure()
        observeSignal()
        addGesture()
        prepareLanguage()
        setupTbv()
        setupSearchBar()
    }
    
    override func configure() {
        if #available(iOS 13, *) {
//            searchBar.searchTextField.backgroundColor = .clear
        } else {
            if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
                textfield.backgroundColor = Color.searchTextFieldBackground()
            }
            searchBar.barTintColor = .white
        }
    }
    
    func observeSignal() {
        inputLanguage.asObservable().subscribe(onNext: { [weak self] (model) in
            guard let self = self else { return }
            let index = self.listLanguage.firstIndex(where: {$0.name == model.name})
            self.markLanguage(index ?? 0)
        }).disposed(by: disposed)
        
        btnDone.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                if !(self.getSelectedLanguage().isEmpty) {
                    self.selectedLanguage.accept((self.selectedIndex, self.getSelectedLanguage()))
                }
                self.animateDetailView(false)
                self.resetSearchBar()
            }).disposed(by: disposed)
    }
    
    func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        vwBound.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap() {
        animateDetailView(false)
        self.resetSearchBar()
    }
    
    func resetSearchBar() {
        self.searchBar.text = ""
        listLanguage = TranslationManager.shared.supportedLanguage
    }
    
}

// MARK: - Support Method
extension ListLanguageView {
    
    private func setupTbv() {
        tbvListLanguage.dataSource = self
        tbvListLanguage.delegate = self
        tbvListLanguage.registerNib(forCell: ListLanguageCell.self)
    }
    
    private func prepareLanguage() {
        listLanguage = TranslationManager.shared.supportedLanguage
    }
    
    private func getSelectedLanguage() -> String {
        return listLanguage.first(where: {$0.selected})?.name ?? ""
    }
    
    /// Animation ListLanguageView
    ///
    /// - Parameters:
    ///   - isShow: Show view when True
    func animateDetailView(_ isShow: Bool = true) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: .curveEaseIn, animations: { [weak self] in
            guard let self = self else { return }
            self.vwLanguage.frame.origin.y = isShow ? self.frame.origin.y + 120 : self.frame.size.height
            self.vwBound.alpha = isShow ? 0.3 : 0.0
            }, completion: { [weak self] _ in
                guard let self = self else { return }
                self.layoutIfNeeded()
                if !(isShow) { self.removeFromSuperview() }
        })
    }
    
}
// MARK: - SearchBar
private extension ListLanguageView {
    func setupSearchBar() {
        searchBar.placeholder = "Search..."
        
        searchBar.rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] (query) in
                guard let self = self else { return }
                self.listLanguage = self.filterLanguage(query)
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    this.tbvListLanguage.reloadData()
                }
            }).disposed(by: disposed)
    }
    
    /// Filter list supported language
    /// - Parameters:
    ///     - query: The input string
    /// - Returns: The new supported language list
    func filterLanguage(_ query: String) -> [LanguageModel] {
        let languages = TranslationManager.shared.supportedLanguage
        guard !query.isEmpty else { return languages }
        var filteredLanguage = [LanguageModel]()
        filteredLanguage = languages.filter({ (language) -> Bool in
            let name = language.name ?? ""
            let searchName = name.lowercased().contains(query.lowercased())
            return searchName
        })
        return filteredLanguage
    }
}

// MARK: - TableView Datasource & Delegate
extension ListLanguageView: UITableViewDataSource, UITableViewDelegate {
    private func modelFor(_ row: Int) -> LanguageModel {
        return listLanguage[safe: row] ?? LanguageModel()
    }
    
    /// Mark the selected language
    ///
    /// - Parameters:
    ///     - index: The index from list language
    private func markLanguage(_ index: Int) {
        listLanguage.forEach { $0.selected = false }
        listLanguage[index].selected = true
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.tbvListLanguage.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listLanguage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: ListLanguageCell.self, at: indexPath)
        cell.loadSupportedLanguage(modelFor(indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        markLanguage(indexPath.row)
    }
    
}
