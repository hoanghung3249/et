//
//  TranslateViewModel.swift
//  ET
//
//  Created by HungNguyen on 11/4/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class TranslateViewModel: BaseViewModel {
    let selectCameraView = CameraViewController()
    let translateText = BehaviorRelay<String?>(value: nil)
    let finalText = BehaviorRelay<String>(value: "")
    var translateModel = TranslateModel()
    var reloadLanguage = PublishSubject<Void>()
    
    override init() {
        super.init()
        selectLanguageView = ListLanguageView()
        
        bindData()
    }
    
    func bindData() {
        swapLanguageBehavior.asObservable().subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.firstSetupLanguage()
            self.swapText()
        }).disposed(by: disposeBag)
        
        reloadLanguage.asObservable().subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.firstSetupLanguage()
        }).disposed(by: disposeBag)
        
        // Combine text and isSupportedVoiceFromLng to show or hide speaker button
        Observable.combineLatest(translateText.asObservable(), isSupportedVoiceFromLng.asObservable()) { (text, isSupported) -> Bool in
            guard let text = text else { return true }
        // If the text is not empty and isSupported = true => Button.isHidden = false
            return !(!(text.isEmpty) && isSupported)
        }.bind(to: isShowSpeakerFromLng).disposed(by: disposeBag)
        
        // Combine result text and isSupportedVoiceToLng to show or hide speaker button
        Observable.combineLatest(finalText.asObservable(), isSupportedVoiceToLng.asObservable()) { (text, isSupported) -> Bool in
            // If the text is not empty and isSupported = true => Button.isHidden = false
            return !(!(text.isEmpty) && isSupported)
        }.bind(to: isShowSpeakerToLng).disposed(by: disposeBag)
    }
        
    func requestTranslateText() {
        guard let sourceText = translateText.value, !sourceText.isEmpty else {
            finalText.accept("")
            return
        }
        translateModel.sourceText = sourceText
        ETServiceManager.shared.request(.translate(translateModel), mapObject: DataModel<ListTranslateModel>.self)
        .trackActivity(activityIndicator)
            .subscribe(onNext: { [weak self] (response) in
                guard let self = self, let model = response.data, let translateResponse = model.translations.first else { return }
                self.finalText.accept(translateResponse.translatedText ?? "")
            }, onError: { [weak self] (error) in
                guard let self = self else { return }
                self.requestError(error)
            }).disposed(by: disposeBag)
    }
    
    func swapText() {
        let sourceText = translateText.value ?? ""
        let targetText = finalText.value
        
        // Swap Text
        translateText.accept(targetText)
        finalText.accept(sourceText)
    }
    
}
