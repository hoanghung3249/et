//
//  CameraViewController.swift
//  ET
//
//  Created by DUY HANDS0ME on 11/25/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CameraViewController: BaseViewController {
    @IBOutlet weak var backView: UIButton!
    @IBOutlet weak var previewVw: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var selectPhotoButton: UIButton!
    @IBOutlet weak var toggleFlashButton: UIButton!
    @IBOutlet weak var swapCameraButton: UIButton!
    @IBOutlet weak var vwLanguage: LanguageView!
    var viewModel = CameraViewModel()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.setupCamera(in: previewVw)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.stopRunningCamera()
    }
    
    override func bindingViewModel() {
        bindCommonAction(viewModel)
        
        viewModel.isFlashOn.asDriver()
            .drive(onNext: { [weak self] (isOn) in
                guard let self = self else { return }
                self.toggleFlashButton.setImage(isOn ? #imageLiteral(resourceName: "FlashOnIcon") : #imageLiteral(resourceName: "FlashOffIcon"), for: .normal)
            }).disposed(by: disposeBag)
        
        selectedImage.bind(to: viewModel.selectedImage).disposed(by: disposeBag)
        
        // Bind data to vwLanguage
        viewModel.fromLanguageBehavior.bind(to: vwLanguage.fromLanguageBehavior).disposed(by: disposeBag)
        viewModel.toLanguageBehavior.bind(to: vwLanguage.toLanguageBehavior).disposed(by: disposeBag)
        
        // Bind action swap language
        vwLanguage.swapLanguageBehavior.bind(to: viewModel.swapLanguageBehavior).disposed(by: disposeBag)
        
        // Bind translateModel
        vwLanguage.translateModel.asObservable()
            .subscribe(onNext: { [weak self] (model) in
                guard let self = self else { return }
                self.viewModel.translateModel = model
            }).disposed(by: disposeBag)
        
        viewModel.selectLanguageView.selectedLanguage.bind(to: vwLanguage.selectedLanguage).disposed(by: disposeBag)
        
        viewModel.translateSourceModel.asObservable()
            .subscribe(onNext: { [weak self] (translateModel) in
                guard let self = self else { return }
                // Pass the data to Translation screen
                let displayTranslateVC = ETStoryboard.items.instantiateViewController(ofType: DisplayTranslateViewController.self)
                displayTranslateVC.viewModel = DisplayTranslateViewModel(translateModel)
                self.navigationPushView(displayTranslateVC)
            }).disposed(by: disposeBag)
    }
    
    override func observeSignal() {
        
        captureButton.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.camera.capturePhoto()
            }).disposed(by: disposeBag)
        
        backView.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.reloadLanguage.onNext(())
                self.navigationPopView()
            }).disposed(by: disposeBag)
        
        selectPhotoButton.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                // Open photo library
                self.presentImagePicker()
            }).disposed(by: disposeBag)
        
        toggleFlashButton.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.isFlashOn.accept(!(self.viewModel.isFlashOn.value))
            }).disposed(by: disposeBag)
        
        swapCameraButton.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.camera.swapCamera()
            }).disposed(by: disposeBag)
        
        vwLanguage.selectLanguageSignal
            .subscribe(onNext: { [weak self] (index, language) in
                guard let self = self else { return }
                self.viewModel.loadSelectLanguageView(in: self.view, index, language)
            }).disposed(by: disposeBag)
    }
}


