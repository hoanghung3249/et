//
//  ExtensionString.swift
//  ET
//
//  Created by HungNguyen on 1/14/20.
//  Copyright © 2020 HungNguyen. All rights reserved.
//

import Foundation

// MARK: - Extensions String
extension String {
    
    func getLanguageCode() -> String {
        let language = self.components(separatedBy: "-")
        guard let languageCode = language.first else { return "" }
        return languageCode
    }
    
}
