//
//  ActionView.swift
//  ET
//
//  Created by HungNguyen on 11/4/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ActionView: BaseCustomView {
    
    @IBOutlet var actionButtons: [UIButton]!
    let selectedSender = PublishRelay<Int>()
    let resetActionButton = PublishSubject<Void>()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
        setupSelectedButton()
        bindData()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
        setupSelectedButton()
        bindData()
    }
    
    override func bindData() {
        resetActionButton.asObservable().subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.setupSelectedButton()
            self.unSelectedButtonExcept(index: 0)
        }).disposed(by: disposed)
    }
    
    private func setupSelectedButton() {
        guard let button = actionButtons.first else { return }
        button.isSelected = true
        setColorSelected(for: button, true)
    }
    
    @IBAction func actionButtonsTap(_ sender: UIButton) {
        // Get the new button
        guard !sender.isSelected else { return }
        // Set selected for new button
        sender.isSelected = !sender.isSelected
        setColorSelected(for: sender, sender.isSelected)
        // Un-select all button except the new one
        unSelectedButtonExcept(index: sender.tag)
        self.selectedSender.accept(sender.tag)
    }
    

    
}

// MARK: - Support Method
private extension ActionView {
    
    /// Set color for selected button
    ///
    /// - Parameters:
    ///      - button: The button
    ///      - isSelected: Bool
    func setColorSelected(for button: UIButton, _ isSelected: Bool) {
        button.setTitleColor(isSelected ? Color.mainColor() : .white
            , for: isSelected ? .selected : .normal)
        button.tintColor = isSelected ? Color.mainColor() : .white
        button.backgroundColor = isSelected ? .white : Color.unSelectedButton()
    }
    
    /// Un-select all button except one
    ///
    /// - Parameters:
    ///      - index: Index of button selected
    func unSelectedButtonExcept(index: Int) {
        let arrButtons = actionButtons.filter({$0.tag != tag})
        arrButtons.forEach { [weak self] (button) in
            guard let self = self else { return }
            button.isSelected = false
            self.setColorSelected(for: button, false)
        }
    }
}
