//
//  CameraViewModel.swift
//  ET
//
//  Created by HungNguyen on 11/27/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CameraViewModel: BaseViewModel {
    var camera: CameraManager!
    var didCapturePhoto = PublishSubject<UIImage?>()
    let isFlashOn = BehaviorRelay<Bool>(value: false)
    var translateModel = TranslateModel()
    let reloadLanguage = PublishSubject<Void>()
    let translateSourceModel = PublishRelay<TranslateModel>()
    
    override init() {
        super.init()
        selectLanguageView = ListLanguageView()
        
        bindData()
    }
    
    func bindData() {
        didCapturePhoto.asObservable().subscribe(onNext: { [weak self] (image) in
            guard let self = self, let image = image else { return }
            self.detectText(from: image)
        }).disposed(by: disposeBag)
        
        isFlashOn.asObservable().subscribe(onNext: { [weak self] (isOn) in
            guard let self = self, self.camera != nil else { return }
            self.camera.toggleFlash(isOn: isOn)
        }).disposed(by: disposeBag)
        
        selectedImage.asObservable().subscribe(onNext: { [weak self] (image) in
            guard let self = self else { return }
            self.detectText(from: image)
        }).disposed(by: disposeBag)
        
        swapLanguageBehavior.asObservable().subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.firstSetupLanguage()
        }).disposed(by: disposeBag)
        
        // Detect text complete
        textFromImage.asObservable().subscribe(onNext: { [weak self] (text) in
            guard let self = self else { return }
            self.translateModel.sourceText = text
            self.translateSourceModel.accept(self.translateModel)
        }).disposed(by: disposeBag)
    }
    
    func setupCamera(in previewView: UIView) {
        camera = CameraManager(previewView)
        camera.didCapturePhoto.bind(to: didCapturePhoto).disposed(by: disposeBag)
    }
    
    func stopRunningCamera() { camera.stopRunning() }
}
