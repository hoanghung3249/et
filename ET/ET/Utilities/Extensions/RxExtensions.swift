//
//  RxExtensions.swift
//  ET
//
//  Created by HungNguyen on 1/8/20.
//  Copyright © 2020 HungNguyen. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: - BehaviorRelay Extensions
extension BehaviorRelay where Element: RangeReplaceableCollection {
    
    func append(_ subElement: Element.Element) {
        var newValue = value
        newValue.append(subElement)
        accept(newValue)
    }
    
    func append(contentsOf: [Element.Element]) {
        var newValue = value
        newValue.append(contentsOf: contentsOf)
        accept(newValue)
    }
    
    public func remove(at index: Element.Index) {
        var newValue = value
        newValue.remove(at: index)
        accept(newValue)
    }
    
    public func removeAll() {
        var newValue = value
        newValue.removeAll()
        accept(newValue)
    }
}
