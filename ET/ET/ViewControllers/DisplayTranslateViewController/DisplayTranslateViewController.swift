//
//  DisplayTranslateViewController.swift
//  ET
//
//  Created by HungNguyen on 12/20/19.
//  Copyright © 2019 HungNguyen. All rights reserved.
//

import UIKit

class DisplayTranslateViewController: BaseViewController {

    @IBOutlet weak var btnVoiceFromLanguage: UIButton!
    @IBOutlet weak var btnVoiceToLanguage: UIButton!
    @IBOutlet weak var btnFromLgTitle: UIButton!
    @IBOutlet weak var btnToLgTitle: UIButton!
    @IBOutlet weak var txvFromLanguage: UITextView!
    @IBOutlet weak var txvToLanguage: UITextView!
    @IBOutlet weak var btnBack: UIButton!
    
    /// View Model
    var viewModel: DisplayTranslateViewModel!
    
    override func configuration() {
        btnVoiceFromLanguage.toCicle()
        btnVoiceToLanguage.toCicle()
    }
    
    override func redrawLayout() {
        setupButtonInTextView()
    }
    
    override func bindingViewModel() {
        bindCommonAction(viewModel)
        
        viewModel.finalText.bind(to: txvToLanguage.rx.text).disposed(by: disposeBag)
        
        viewModel.translateModel.asObservable()
            .subscribe(onNext: { [weak self] (model) in
                guard let self = self, let model = model else { return }
                self.txvFromLanguage.text = model.sourceText
                self.btnFromLgTitle.setTitle(self.getLanguageName(from: model.sourceLanguage), for: .normal)
                self.btnToLgTitle.setTitle(self.getLanguageName(from: model.target), for: .normal)
            }).disposed(by: disposeBag)
    }
    
    override func observeSignal() {
        btnBack.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.navigationPopView()
        }).disposed(by: disposeBag)
        
        btnVoiceFromLanguage.rx.tap.subscribe(onNext: {
            print("Speech from language")
        }).disposed(by: disposeBag)
        
        btnVoiceToLanguage.rx.tap.subscribe(onNext: {
            print("Speech to language")
        }).disposed(by: disposeBag)
    }
    
}

// MARK: - Support method
private extension DisplayTranslateViewController {
    
    func setupButtonInTextView() {
        // Get the button frame from storyboard
        let buttonFromLgFrame = btnFromLgTitle.frame
        let buttonToLgFrame = btnToLgTitle.frame
        
        // Convert button's frame to path
        let exclusiveFromLgPath = UIBezierPath(rect: buttonFromLgFrame)
        let exclusiveToLgPath = UIBezierPath(rect: buttonToLgFrame)
        
        // Add path to the textview in main queue
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.txvFromLanguage.textContainer.exclusionPaths = [exclusiveFromLgPath]
            self.txvToLanguage.textContainer.exclusionPaths = [exclusiveToLgPath]
        }
    }
    
}
